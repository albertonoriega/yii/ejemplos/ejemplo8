<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ArrayDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        
        $vectorNoticias= $this->leerNoticias("https://www.eldiariomontanes.es/rss/2.0/?section=ultima-hora");
        
        // vamos a mostrar todas las noticias en un GRIDVIEW
        $dataProvider= new ArrayDataProvider([
            "allModels"=> $vectorNoticias,
            // Paginacion de 10 en 10 (hay que hacerlo en el Data Provider)
            'pagination' => [
                'pageSize' => 10
            ],
        ]);
        //var_dump($noticias);
        return $this->render('index',[
            "dataProvider" => $dataProvider,
            "titulo" => "Noticias de última hora",
            
        ]);
    }

    
     public function actionCine() {
        $vectorNoticias= $this->leerNoticias("https://www.eldiariomontanes.es/rss/2.0/?section=butaca/cine");
        
        // vamos a mostrar todas las noticias en un GRIDVIEW
        $dataProvider= new ArrayDataProvider([
            "allModels"=> $vectorNoticias,
            // Paginacion de 10 en 10 (hay que hacerlo en el Data Provider)
            'pagination' => [
                'pageSize' => 10
            ],
        ]);
        //var_dump($noticias);
        return $this->render('index',[
            "dataProvider" => $dataProvider,
            "titulo" => "Cine",
            
        ]);
    }
    
    public function actionRecetas() {
        // usamos el metodo que hemos creado abajo
        $vectorNoticias= $this->leerNoticias("https://www.eldiariomontanes.es/rss/2.0/?section=cantabria-mesa/recetas");
         
        
        // vamos a mostrar todas las noticias en un GRIDVIEW
        $dataProvider= new ArrayDataProvider([
            "allModels"=> $vectorNoticias,
            // Paginacion de 10 en 10 (hay que hacerlo en el Data Provider)
            'pagination' => [
                'pageSize' => 10
            ],
        ]);
        //var_dump($noticias);
        return $this->render('index',[
            "dataProvider" => $dataProvider,
            "titulo" => "Recetas",
            
        ]);
    }
    /**
     * Creamos un metodo que te pasan una url y te devuelve un array con las noticias de dicho url
     * @param type $url Es la direccion del servidor RSS
     * @return \app\models\Noticias()
     */
    
    private function leerNoticias ($url){
                
        // Leer una página web
        $contenido = file_get_contents($url);
        //var_dump($contenido);
        // funcion de php que convierte un xml a un objeto
        // en channel->item es donde estan las noticias
        // Esto te daría todas las noticias pero solo te devuelve la primera
        $objetoXML=simplexml_load_string($contenido)->channel->item;
        
        
        // Clonamos el objeto a nuestro modelo
        foreach ($objetoXML as $noticia) {
            // Noticia es un objeto de tipo XMLElement
            
            // ObjetoMioNoticia es un modelo, es un objeto creado a partir de la clase Noticias
            $objetoMioNoticia = new \app\models\Noticias();
            // sin utilizar asignacion masiva
//            $ObjetoMioNoticia->title=$noticia->title;
//            $ObjetoMioNoticia->link=$noticia->link;
//            $ObjetoMioNoticia->description=$noticia->description;
            
            // Con asignacion masiva
            $objetoMioNoticia->attributes=(array)$noticia; // hacemos un cast para convetir el objeto a un array
            
            // Me voy creando un array con objetos de tipo Noticias
            $vectorNoticias[]=$objetoMioNoticia;
        }
        return $vectorNoticias;
    }

    
}
