<?php

namespace app\models;

use yii\base\Model;

class Noticias extends Model {

    // Propiedades
    public string $title;
    public string $link;
    public string $description;
    
    // Los labels (etiquetas) que quiero que muestren los widget
    public function attributeLabels() {
        return [
            "title" => "Título",
            "link" => "Enlace",
            "description" => "Descripción",
        ];
    }
    
    public function rules(): array {
        return [
         // Para poder usar la asignacion masiva las propiedades tienen que ser seguras
          [['title','description','link'],'safe']  
        ];
    }
        
    
    
    // Metodo para crear un enlace con un boton
    public function crearEnlace(){
        return \yii\helpers\Html::a(
                "Ver noticia",
                $this->link,
                ["class" => "btn btn-primary"]
        );
    }
    
    // Metodo para poner la imagen de un XML
    public function crearImagen() {
        return str_replace(
                "src",
                'class="img-thumbnail" src',
                $this->description
                );
        
    }
    public function getEnlace(){
        return \yii\helpers\Html::a(
                "Ver noticia",
                $this->link,
                ["class" => "btn btn-primary"]
        );
    }
    
    public function getDescripcion() {
        return str_replace(
                "src",
                'class="img-thumbnail" src',
                $this->description
                );
        
    }
    
    
}
