<?php

/* @var $this yii\web\View */

$this->title = $titulo;
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4"><?= $this->title?></h1>

    </div>

    <?= 
    \yii\grid\GridView::widget([
        "dataProvider" => $dataProvider,
        "columns" => [
            
            "title", // colocando directamente una propiedad del modelo
            // colocar un metodo del modelo
            [
                'label' => 'Enlace',
                'content' => function ($model){
                    return $model->crearEnlace();
                } 
            ],
            // colocar un metodo del modelo
            [
                'label' => 'Imagen',
                'content' => function ($model){
                   return $model->crearImagen();
                }
            ],
            // colocar un getter del modelo
            [
                'attribute'=> 'enlace', // como no encuentra la propiedad enlace (porque no existe) llama al getter que hemos creado
                'format'=> 'raw',
            ],
            [
                'attribute' => 'descripcion', // igual que en el enlace llamamos al getter llamado descripcion
                'format' => 'raw',
            ]
        ]
    ]);
    ?>
</div>
